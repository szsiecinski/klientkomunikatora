package szysie.klient;
import java.io.*;
public class KlientMain {

	public static void main(String[] args) {
		System.out.println("Klient komunikatora");
		
		int port = 10105;
		String adres = "localhost";
		
		if(args.length == 1) {
			adres = args[0];
		}
		
		if (args.length == 2) {
			adres = args[0];
			port = Integer.parseInt(args[1]);
		}
		
		System.out.println("Aby skorzystać z komunikatora, podaj nazwę użytkownika.\nAby zakończyć pracę, wpisz exit.");
		System.out.println("Podaj nazwę użytkownika");
		
		class ObslugaUzytkownika implements Runnable {
			private int port;
			private String adres;
			private String pomoc;
			
			public ObslugaUzytkownika(String adres, int port) {
				this.port = port;
				this.adres = adres;
				
				pomoc = "\nKomendy: send - wyślij wiadomość użytkownikowi LOGIN.\nsendall - wyślij wiadomość wszystkim";
				pomoc += "\nget - odbierz wiadomości\ngetall - odbierz wszystkie wiadomości\narcv - automatyczne odbieranie";
				pomoc += "\nlist - pokaż wszystkich zalogowanych użytkowników\nexit - zakończ pracę.";
			}
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Klient k = Klient.UtworzKlienta(adres,port);
					
					//bufor tekstowy
					BufferedReader bufor = new BufferedReader(new InputStreamReader(System.in));
					
					//wczytanie nazwy uzytkownika
					String nazwa_uzytkownika = bufor.readLine();

					k.zalogujSie(nazwa_uzytkownika);
					System.out.println("Możesz pracować.");
					System.out.println(pomoc);
					
					while(true) {
						String komenda = bufor.readLine();
						
						if(komenda.toUpperCase().equals("LIST")) {
							k.pokazZalogowanychUzytkownikow();
						}
						
						if(komenda.toUpperCase().equals("ARCV")) {
							String decyzja;
							do {
							System.out.println("Aby włączyć, wpisz T. Aby wyłączyć, wpisz N");
							decyzja = bufor.readLine();
							
							if(decyzja.toUpperCase().equals("T"))
								k.autoOdbieranieWiadomosci(true);
							else
								k.autoOdbieranieWiadomosci(false);
							} while(decyzja.equals("T") || decyzja.equals("N"));
						}
						
						if(komenda.toUpperCase().equals("GET")) {
							System.out.println("Podaj nazwę nadawcy wiadomości do ciebie");
							String login = bufor.readLine();
							k.odbierzWiadomosci(login);
						}
						
						if(komenda.toUpperCase().equals("GETALL")) {
							
							k.odbierzWszystkieWiadomosci();
						}
						
						if(komenda.toUpperCase().equals("SEND")) {
							System.out.println("Podaj nazwę użytkownika, do którego chcesz wysłać wiadomość");
							String login = bufor.readLine();
							System.out.println("Wpisz treść wiadomości\n>");
							String tresc = bufor.readLine();
							
							k.wyslijWiadomosc(login, tresc);
						}
						
						if(komenda.toUpperCase().equals("SENDALL")) {
							System.out.println("Wpisz treść wiadomości");
							String tresc = bufor.readLine();
							
							k.wyslijWiadomoscWszystkim(tresc);
						}
							
						if(komenda.toUpperCase().equals("EXIT")) {
							k.zakonczSesje();
							System.out.println("Sesja zakończona");
							break;
						}
						
						k.odbierajWiadomosci();
					}
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}	
		}
		
		//obsługa wątków komunikatora
//		java.util.concurrent.Executor watki_kom = java.util.concurrent.Executors.newCachedThreadPool();
//		watki_kom.execute(new ObslugaUzytkownika(adres,port));
		new Thread(new ObslugaUzytkownika(adres,port)).run();
	}
}
