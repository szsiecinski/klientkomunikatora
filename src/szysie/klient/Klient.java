package szysie.klient;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Klient {
	private static Klient k;
	private Socket gniazdo;
	
	private DataOutputStream wyjscieDanych;
	private BufferedReader daneSerwera;
	
	private Executor watkiOdbioru;
	
	protected Klient(String adres, int port) throws IOException
	{
		gniazdo = new Socket(adres, port);
		wyjscieDanych = new DataOutputStream(gniazdo.getOutputStream());
		daneSerwera = new BufferedReader(new InputStreamReader(gniazdo.getInputStream()));
		watkiOdbioru = Executors.newCachedThreadPool();
	}
	
	public static Klient UtworzKlienta(String adres, int port) {
		if(k == null) {
			try {
				return new Klient(adres,port);
			}
			catch (IOException e){
				return null;
			}
		}
		else {
			return k;
		}
	}
	
	public void zalogujSie(String nazwa_uzytkownika) throws IOException {
		wyjscieDanych.writeBytes("LOGIN " + nazwa_uzytkownika + "\n");
		wyjscieDanych.writeBytes("AUTORECEIVE TRUE\n");
	}
	
	public void pokazZalogowanychUzytkownikow() throws IOException {
		wyjscieDanych.writeBytes("LISTCLIENTS\n");
		
		String odpowiedz;
		while((odpowiedz = daneSerwera.readLine()) != null) {
			String uzytkownicy = "";
			String[] uzytkownikyArray = odpowiedz.split(";");
			
			for(String uzytkownik : uzytkownikyArray)
			{
				uzytkownicy += uzytkownik + ", ";
			}
			
			System.out.printf("Zalogowani użytkownicy: %s\n", uzytkownicy);
			break;
		}
		
	}
	
	public void autoOdbieranieWiadomosci(boolean stan) throws IOException {
		
		if(stan)
			wyjscieDanych.writeBytes("AUTORECEIVE TRUE\n");
		else
			wyjscieDanych.writeBytes("AUTORECEIVE FALSE\n");
	}
	
	public void zakonczSesje() throws IOException {
		wyjscieDanych.writeBytes("EXIT");
		gniazdo.close();
	}
	
	public void odbierzWiadomosci(String login) throws IOException {
		wyjscieDanych.writeBytes(String.format("GET %s\n",login));
		
		String odpowiedz;
		while((odpowiedz = daneSerwera.readLine()) != null) {
			System.out.printf("Wiadomości od użytkownika %s:\n %s\n", login, odpowiedz);
			break;
		}
	}
	
	public void odbierzWszystkieWiadomosci() throws IOException {
		wyjscieDanych.writeBytes("GETALL %s\n");
		String odpowiedz;
		while((odpowiedz = daneSerwera.readLine()) != null) {
			System.out.printf("Wszystkie wiadomości:\n %s\n", odpowiedz);
			break;
		}
	}
	
	public void wyslijWiadomosc(String login, String tresc) throws IOException {
		wyjscieDanych.writeBytes(String.format("SEND %s %s\n", login, tresc));
		//System.out.printf("%s\n", daneSerwera.readLine());
	}
	
	public void wyslijWiadomoscWszystkim(String tresc) throws IOException {
		wyjscieDanych.writeBytes(String.format("SENDALL %s\n", tresc));
		//System.out.printf("%s\n", daneSerwera.readLine());
	}
	
	public void odbierajWiadomosci() throws IOException {
		
		class OdbieranieWiadomosci implements Runnable {
			private BufferedReader strumienSerwera;
			
			public OdbieranieWiadomosci(BufferedReader strumienSerwera) {
				this.strumienSerwera = strumienSerwera;
			}
			
			@Override
			public void run() {
				String wyjscie;

				try {
					while ((wyjscie = strumienSerwera.readLine()) != null) {

						if(wyjscie.equals("") == false) {
							this.notify();
							System.out.printf("\n%s\n",wyjscie);
						}

					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		watkiOdbioru.execute(new OdbieranieWiadomosci(daneSerwera));
	}
}
